#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ExerciseOne.h"

class ExerciseOne : public QMainWindow
{
	Q_OBJECT

public:
	explicit ExerciseOne(QWidget *parent = Q_NULLPTR);
signals:
	
	void clicked();
private slots:
	void handleButton();
	QString getText(QLineEdit *sourceLine);
private:
	Ui::ExerciseOneClass ui;
};
