/********************************************************************************
** Form generated from reading UI file 'ExerciseOne.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXERCISEONE_H
#define UI_EXERCISEONE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ExerciseOneClass
{
public:
    QWidget *centralWidget;
    QLineEdit *source;
    QPushButton *button;
    QLineEdit *source_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ExerciseOneClass)
    {
        if (ExerciseOneClass->objectName().isEmpty())
            ExerciseOneClass->setObjectName(QStringLiteral("ExerciseOneClass"));
        ExerciseOneClass->resize(600, 400);
        centralWidget = new QWidget(ExerciseOneClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        source = new QLineEdit(centralWidget);
        source->setObjectName(QStringLiteral("source"));
        source->setGeometry(QRect(142, 60, 331, 20));
        button = new QPushButton(centralWidget);
        button->setObjectName(QStringLiteral("button"));
        button->setGeometry(QRect(270, 140, 75, 23));
        button->setCheckable(true);
        source_2 = new QLineEdit(centralWidget);
        source_2->setObjectName(QStringLiteral("source_2"));
        source_2->setGeometry(QRect(140, 230, 331, 20));
        ExerciseOneClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ExerciseOneClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        ExerciseOneClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(ExerciseOneClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        ExerciseOneClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(ExerciseOneClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ExerciseOneClass->setStatusBar(statusBar);

        retranslateUi(ExerciseOneClass);

        QMetaObject::connectSlotsByName(ExerciseOneClass);
    } // setupUi

    void retranslateUi(QMainWindow *ExerciseOneClass)
    {
        ExerciseOneClass->setWindowTitle(QApplication::translate("ExerciseOneClass", "ExerciseOne", nullptr));
        button->setText(QApplication::translate("ExerciseOneClass", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ExerciseOneClass: public Ui_ExerciseOneClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXERCISEONE_H
