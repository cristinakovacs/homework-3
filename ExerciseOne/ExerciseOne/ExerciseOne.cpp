#include "ExerciseOne.h"

ExerciseOne::ExerciseOne(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.button, SIGNAL(clicked()), this, SLOT(handleButton()));
}

QString ExerciseOne::getText(QLineEdit *source) {
	QString myString;
	myString = source->text();
	return myString;

}

void ExerciseOne::handleButton()
{
	ui.source_2->setText(getText(ui.source));
	emit ui.source_2;
	ui.source->clear();
}
