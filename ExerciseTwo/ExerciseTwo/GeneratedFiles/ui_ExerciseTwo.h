/********************************************************************************
** Form generated from reading UI file 'ExerciseTwo.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXERCISETWO_H
#define UI_EXERCISETWO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ExerciseTwoClass
{
public:
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QSlider *redSlider;
    QSlider *greenSlider;
    QSlider *blueSlider;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *redLabel;
    QLabel *greenLabel;
    QLabel *blueLabel;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ExerciseTwoClass)
    {
        if (ExerciseTwoClass->objectName().isEmpty())
            ExerciseTwoClass->setObjectName(QStringLiteral("ExerciseTwoClass"));
        ExerciseTwoClass->resize(600, 400);
        centralWidget = new QWidget(ExerciseTwoClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 9, 321, 321));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        redSlider = new QSlider(verticalLayoutWidget);
        redSlider->setObjectName(QStringLiteral("redSlider"));
        redSlider->setAutoFillBackground(false);
        redSlider->setMaximum(255);
        redSlider->setValue(0);
        redSlider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(redSlider);

        greenSlider = new QSlider(verticalLayoutWidget);
        greenSlider->setObjectName(QStringLiteral("greenSlider"));
        greenSlider->setAutoFillBackground(false);
        greenSlider->setMaximum(255);
        greenSlider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(greenSlider);

        blueSlider = new QSlider(verticalLayoutWidget);
        blueSlider->setObjectName(QStringLiteral("blueSlider"));
        blueSlider->setAutoFillBackground(false);
        blueSlider->setMaximum(255);
        blueSlider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(blueSlider);

        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(369, 9, 191, 321));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        redLabel = new QLabel(verticalLayoutWidget_2);
        redLabel->setObjectName(QStringLiteral("redLabel"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        redLabel->setFont(font);

        verticalLayout_2->addWidget(redLabel);

        greenLabel = new QLabel(verticalLayoutWidget_2);
        greenLabel->setObjectName(QStringLiteral("greenLabel"));
        greenLabel->setFont(font);

        verticalLayout_2->addWidget(greenLabel);

        blueLabel = new QLabel(verticalLayoutWidget_2);
        blueLabel->setObjectName(QStringLiteral("blueLabel"));
        blueLabel->setFont(font);

        verticalLayout_2->addWidget(blueLabel);

        ExerciseTwoClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ExerciseTwoClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        ExerciseTwoClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(ExerciseTwoClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        ExerciseTwoClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(ExerciseTwoClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ExerciseTwoClass->setStatusBar(statusBar);

        retranslateUi(ExerciseTwoClass);

        QMetaObject::connectSlotsByName(ExerciseTwoClass);
    } // setupUi

    void retranslateUi(QMainWindow *ExerciseTwoClass)
    {
        ExerciseTwoClass->setWindowTitle(QApplication::translate("ExerciseTwoClass", "ExerciseTwo", nullptr));
        redLabel->setText(QApplication::translate("ExerciseTwoClass", "RED", nullptr));
        greenLabel->setText(QApplication::translate("ExerciseTwoClass", "GREEN", nullptr));
        blueLabel->setText(QApplication::translate("ExerciseTwoClass", "BLUE", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ExerciseTwoClass: public Ui_ExerciseTwoClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXERCISETWO_H
