#include "ExerciseTwo.h"

ExerciseTwo::ExerciseTwo(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	setWindowTitle("RGB Background");
	QMainWindow* myWindow = new QMainWindow(this);

	connect(ui.redSlider, SIGNAL(valueChanged(int)), this, SLOT(setRedValue(int)));
	connect(ui.greenSlider, SIGNAL(valueChanged(int)), this, SLOT(setGreenValue( int)));
	connect(ui.blueSlider, SIGNAL(valueChanged(int)), this, SLOT(setBlueValue(int)));

}
ExerciseTwo::~ExerciseTwo(){

}


void ExerciseTwo::setRedValue(int val)
{
	emit valueChanged(val);
	changeColor("red", val);
}
void ExerciseTwo::setGreenValue(int val)
{
	emit valueChanged(val);
	changeColor("green", val);
}
void ExerciseTwo::setBlueValue(int val)
{
	emit valueChanged(val);
	changeColor("blue", val);

}

void ExerciseTwo::changeColor(std::string text, int val) {

	if (text == "red") {
		red = val;
	}
	if(text=="green") {
		green = val;
	}
	if (text == "blue") {
		blue = val;
	}
	myColor.setRgb(red, green, blue);
	pal.setColor(QPalette::Background, myColor);
	this->setAutoFillBackground(true);
	this->setStyleSheet("{background: 'myColor';}");
	this->setPalette(pal);
	this->show();
	
	}
