#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ExerciseTwo.h"
#include <QWidget>
#include<qlineedit.h>
#include<qslider.h>
#include<string.h>
#include<qfiledialog.h>
#include<qwindow.h>
class ExerciseTwo : public QMainWindow
{
	Q_OBJECT

public:
	ExerciseTwo(QWidget *parent = Q_NULLPTR);
	~ExerciseTwo();
	int red=0;
	int green=0;
	int blue=0;
	QColor myColor;
	QPalette pal = palette();
signals:
	void valueChanged( int value);
public slots:
	void setRedValue( int val);
	void setGreenValue(int val);
	void setBlueValue(int val);
	void changeColor(std::string text, int val);

public:
	Ui::ExerciseTwoClass ui;	

};
