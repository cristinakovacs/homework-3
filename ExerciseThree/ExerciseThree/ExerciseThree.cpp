#include "ExerciseThree.h"


ExerciseThree::ExerciseThree(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.pushButton_2, SIGNAL(clicked()), this, SLOT(handleButton()));
}
ExerciseThree::~ExerciseThree() {
	ExerciseThree::close();
}

void ExerciseThree::handleButton() {

	QLineEdit *line = new QLineEdit;
	line->setObjectName("line" + index);
	line->setText("new line");
	ui.verticalLayout->addWidget(line, 0); //this line is used to insert lineEdit under our button
	//ui.verticalLayout->insertWidget(0, line); //and this line may be used to insert element on top of our button
	index++;
	line->close();
	}

