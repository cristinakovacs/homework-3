#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ExerciseThree.h"
#include <QMainWindow>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QList>
#include<qscrollbar.h>

class ExerciseThree : public QMainWindow
{
	Q_OBJECT

public:
	ExerciseThree(QWidget *parent = Q_NULLPTR);
	~ExerciseThree();
	int index=0;
signals:
	void clicked();
public slots:
	void handleButton();

private:
	Ui::ExerciseThreeClass ui;
};
