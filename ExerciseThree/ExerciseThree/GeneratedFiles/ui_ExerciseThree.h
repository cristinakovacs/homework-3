/********************************************************************************
** Form generated from reading UI file 'ExerciseThree.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXERCISETHREE_H
#define UI_EXERCISETHREE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ExerciseThreeClass
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ExerciseThreeClass)
    {
        if (ExerciseThreeClass->objectName().isEmpty())
            ExerciseThreeClass->setObjectName(QStringLiteral("ExerciseThreeClass"));
        ExerciseThreeClass->resize(337, 443);
        centralWidget = new QWidget(ExerciseThreeClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(6, -1, 6, -1);
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);


        gridLayout->addLayout(verticalLayout, 0, 1, 1, 1);

        ExerciseThreeClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ExerciseThreeClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 337, 21));
        ExerciseThreeClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(ExerciseThreeClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        ExerciseThreeClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(ExerciseThreeClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ExerciseThreeClass->setStatusBar(statusBar);

        retranslateUi(ExerciseThreeClass);

        QMetaObject::connectSlotsByName(ExerciseThreeClass);
    } // setupUi

    void retranslateUi(QMainWindow *ExerciseThreeClass)
    {
        ExerciseThreeClass->setWindowTitle(QApplication::translate("ExerciseThreeClass", "ExerciseThree", nullptr));
        pushButton_2->setText(QApplication::translate("ExerciseThreeClass", "Add a new line edit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ExerciseThreeClass: public Ui_ExerciseThreeClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXERCISETHREE_H
